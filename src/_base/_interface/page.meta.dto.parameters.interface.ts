import { PageOptionsDto } from "../_dto/page.options.dto";

export interface IPageMetaDtoParameters {
    pageOptionsDto: PageOptionsDto;
    itemCount: number;
  }